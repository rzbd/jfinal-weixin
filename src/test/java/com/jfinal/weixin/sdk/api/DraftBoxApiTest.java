/*
 * Copyright (c) 2011-2021, L.cm (596392912@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.sdk.api;

import com.jfinal.json.JsonManager;
import org.junit.Before;
import org.junit.Test;

/**
 * 草稿箱
 * <p>
 * 文档地址：https://developers.weixin.qq.com/doc/offiaccount/Draft_Box/Add_draft.html
 *
 * @author L.cm
 */
public class DraftBoxApiTest {

    @Before
    public void setUp() {
        AccessTokenApiTest.init();
        JsonManager.me().setDefaultJsonFactory(new com.jfinal.json.MixedJsonFactory());
    }

    @Test
    public void count() {
        ApiResult apiResult = DraftBoxApi.count();
        System.out.println(apiResult);
    }

}
